const mongoose = require("mongoose");
const validator = require("validator");
var leaveDataSchema = new mongoose.Schema({
  user_id: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: "gmailusers"
  },
  leave_type: {
    type: String,
    required: true
  },
  leave_dates: [],
  leave_message: {
    type: String,
    required: true
  },
  leave_poc: [],
  color: {
    type: String,
    required: true
  },
  leave_status: {
    type: String,
    required: true
  },
  year: {
    type: String,
    required: true
  },
  approved_by: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "gmailusers"
  },
  cancelled_by: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "gmailusers"
  },
  cancellation_reason: {
    type: String
  },
  isCancelProcess: {
    type: Boolean
  },
  team: {
    type: String
  }
});

leaveDataSchema.set("timestamps", true);

const LeaveRegister = mongoose.model("leaveregister", leaveDataSchema);

module.exports = LeaveRegister;
