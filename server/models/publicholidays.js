const mongoose = require('mongoose');
const validator = require('validator')
var publicHolidayDataSchema = new mongoose.Schema({
    date: {
        type: String,
        unique: true,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    location: [
        {
            city: {
                type: String,
                required: true
            }
        }
    ]
});

publicHolidayDataSchema.set('timestamps', true)

const PublicHolidays = mongoose.model('public_holidays', publicHolidayDataSchema)

module.exports = PublicHolidays
