const mongoose = require("mongoose");
var feedbackDataSchema = new mongoose.Schema({
  user_id: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: "gmailusers"
  },
  feedback: {
    type: String,
    required: true
  }
});

feedbackDataSchema.set("timestamps", true);

const Feedback = mongoose.model("feedbacks", feedbackDataSchema);

module.exports = Feedback;
