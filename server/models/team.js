const mongoose = require("mongoose");
const validator = require("validator");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
var teamDataSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    minlength: 2,
    trim: true,
    lowercase: false
  },
  email: {
    type: String,
    index: true,
    unique: true,
    required: true,
    minlength: 3,
    trim: true,
    lowercase: true,
    validate(value) {
      if (!validator.isEmail(value)) {
        throw new Error("Email is invalid");
      }
    }
  },
  team: {
    type: String,
    required: true,
    trim: true
  },
  designation: {
    type: String,
    required: true,
    trim: true
  },
  designation_text: {
    type: String,
    required: true,
    trim: true
  },
  designation_order: {
    type: String,
    trim: true
  },
  write_up: {
    type: String,
    required: true
  },
  page_order: {
    type: String,
    required: true
  },
  profile_avatar_url: {
    type: String
  },
  team_class: [],
  status: {
    type: Boolean
  }
});

teamDataSchema.set("timestamps", true);

const Team = mongoose.model("team", teamDataSchema);

module.exports = Team;
