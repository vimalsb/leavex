const mongoose = require('mongoose');
var leadsDataSchema = new mongoose.Schema({
    name: {
        type: String
    },
    email: {
        type: String
    },
    phone: {
        type: String
    }
});

leadsDataSchema.set('timestamps', true)

const Leads = mongoose.model('leads', leadsDataSchema)

module.exports = Leads
