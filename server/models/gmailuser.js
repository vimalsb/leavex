const mongoose = require("mongoose");
const validator = require("validator");
const jwt = require("jsonwebtoken");
var gUserDataSchema = new mongoose.Schema({
  googleId: {
    type: String,
    required: true,
    minlength: 3,
    trim: true
  },
  employeeID: {
    type: String,
    trim: true
  },
  exitDate: {
    type: String
  },
  gender: {
    type: String
  },
  doe: {
    type: String
  },
  role: {
    type: String,
    required: true
  },
  carryForwardLeaves: [
    { year: { type: String }, isCarryForward: { type: Boolean } }
  ],
  userName: {
    type: String,
    required: true,
    trim: true
  },
  userEmail: {
    type: String,
    required: true,
    trim: true
  },
  picture: {
    type: String
  },
  token: {
    type: String
  },
  app_id: [
    {
      token: {
        type: String
      }
    }
  ],
  leavedates: [
    {
      leave_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "leaveregister"
      },
      approved_by: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "gmailusers"
      }
    }
  ],
  settings: [
    {
      reporting_person: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "gmailusers"
      },
      phone_number: {
        type: String,
        trim: true
      },
      working_city: {
        type: String,
        trim: true
      },
      dob: {
        type: String
      },
      blood_group: {
        type: String
      },
      team: {
        type: String
      },
      designation: {
        type: String
      },
      about_me: {
        type: String
      },
      doj: {
        type: String
      },
      page_position: {
        type: String
      }
    }
  ],
  profile_status: {
    type: Boolean
  },
  buddies: [
    {
      buddy_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "gmailusers"
      }
    }
  ]
});

gUserDataSchema.methods.generateAuthToken = async function() {
  const user = this;
  const token = jwt.sign({ _id: user._id.toString() }, "leavexv1");

  user.token = token;
  await user.save();

  return token;
};

gUserDataSchema.set("timestamps", true);

const Guser = mongoose.model("gmailusers", gUserDataSchema);

module.exports = Guser;
