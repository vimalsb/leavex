const User = require('../models/gmailuser')
var jwt = require('jsonwebtoken')

const auth = async (req, res, next) => {
    try {
        const token = req.body.token
        const decoded = jwt.verify(token, 'leavexv1');
        var user = await User.findById(decoded._id);
        if (!user) {
            throw new Error()
        }
        req.token = token
        req.user = user
        next()
    } catch (e) {
        res.status(400).send()
    }
}

module.exports = auth
