const userAuth = async (req, res, next) => {
  const token = req.header("Authorization").replace("Bearer ", "");
  console.log(token);
  next();
};

module.exports = userAuth;
