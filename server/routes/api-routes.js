const { Router } = require("express");
const mongoose = require("mongoose");
var ObjectId = require("mongoose").Types.ObjectId;
const { OAuth2Client } = require("google-auth-library");
const client = new OAuth2Client(
  "843335273338-dnd4afvfj4q4s31sakenr9g228flmm1p.apps.googleusercontent.com"
);
const Guser = require("../models/gmailuser.js");
const Team = require("../models/team.js");
const LeaveRegister = require("../models/leaveregister.js");
const PublicHolidays = require("../models/publicholidays.js");
const Leads = require("../models/leads.js");
const Feedback = require("../models/feedback.js");
const send_leave_apply_mail = require("../modules/send_apply_leave_mail");
const send_cancel_mail_req = require("../modules/send_cancel_mail_req");
const send_leave_status = require("../modules/send_leave_status");
const send_common_mail = require("../modules/send_common_mail");
const router = Router();
const db =
  "mongodb+srv://leavexadmin:leavex_v1_social_326@leavex-jwyuu.mongodb.net/test?retryWrites=true&w=majority";
const jwt = require("jsonwebtoken");
const auth = require("../auth/auth");
const aws = require("aws-sdk");
const multer = require("multer");
const multerS3 = require("multer-s3");
const csv = require("csvtojson");
const request = require("request");
var moment = require("moment-timezone");
const nodemailer = require("nodemailer");

let converter = require("json-2-csv");
var uniqid = require("uniqid");
var fs = require("fs");
const path = require("path");
const userAuth = require("../auth/header");

aws.config.update({
  secretAccessKey: "+hKGEqePGfaeVNiWun/S8zTXAEOv7mjS9BeFReER",
  accessKeyId: "AKIA24PGISHBKO3ZGHQY",
  region: "ap-south-1"
});

var s3 = new aws.S3();

var upload = multer({
  storage: multerS3({
    s3: s3,
    bucket: "socialbeatoneteamonebeat",
    acl: "public-read",
    key: function(req, file, cb) {
      console.log(file);
      console.log(Date.now() + file.originalname);
      cb(null, Date.now() + file.originalname); //use Date.now() for unique file keys
    }
  })
});

mongoose.connect(db, (err, res) => {
  if (err) {
    console.log("Failed to connected to " + db);
  } else {
    console.log("Connected to " + db);
  }
});

router.post("/notifications/assign-user", async (req, res) => {
  var token = req.body.token;
  var app_id = req.body.appid;
  try {
    const decoded = jwt.verify(token, "leavexv1");
    var user = await Guser.findById(decoded._id);
    if (!user) {
      return res.status(404).send();
    }
    try {
      var dToken = false;
      if (user.app_id) {
        for (let i = 0; i < user.app_id.length; i++) {
          if (user.app_id[i].token == app_id) {
            dToken = true;
          }
        }
      }
      if (dToken === false) {
        user.app_id.push({ token: app_id });
        await user.save();
        return res.send();
      }
      res.send();
    } catch (error) {
      return res.status(400).send(error);
    }
  } catch (error) {
    res.status(400).send(error);
  }
});

router.post("/public-holidays/add", async (req, res) => {
  var date = req.body.date;
  var name = req.body.name;
  var location = req.body.location;
  var newHoliday = new PublicHolidays({
    date,
    name,
    location
  });
  await newHoliday.save();
  res.send(newHoliday);
});

router.get("/public-holidays/get", async (req, res) => {
  var holidays = await PublicHolidays.find();
  res.send(holidays);
});

router.post("/logout", (req, res) => {
  res.clearCookie("leavetoken");
  req.logout();
  res.send();
});

router.post("/chech-g-token", async (req, res) => {
  var token = req.body.token;
  var admin_emails = [
    "vikas@socialbeat.in",
    "suneil@socialbeat.in",
    "arju@socialbeat.in"
  ];
  if (!token) {
    return res.send({ decoded: false, loggedin: false, token: false });
  }
  try {
    const ticket = await client.verifyIdToken({
      idToken: token,
      audience:
        "843335273338-dnd4afvfj4q4s31sakenr9g228flmm1p.apps.googleusercontent.com"
    });
    const payload = ticket.getPayload();
    const userid = payload["sub"];
    const domain = payload["hd"];
    const email = payload["email"];
    const name = payload["name"];
    const picture = payload["picture"];
    if (domain != "socialbeat.in") {
      return res.send({ decoded: false, loggedin: false });
    }
    var user = await Guser.findOne({ googleId: userid });
    if (!user) {
      var newUser = new Guser({
        googleId: userid,
        userEmail: email,
        userName: name,
        picture: picture,
        role: admin_emails.includes(email) === true ? "1" : "4",
        profile_status: false,
        employeeID: "",
        carryForwardLeaves: []
      });
      try {
        await newUser.save();
        var token = newUser.generateAuthToken();
        return res.send({ decoded: true, loggedin: true, user: newUser });
      } catch (e) {
        console.log(e);
        return res.send({ decoded: false, loggedin: false });
      }
    }
    res.send({ decoded: true, loggedin: true, user: user });
  } catch (error) {
    console.log(error);
    res.send({ decoded: false, loggedin: false });
  }
});

router.post("/leave/view-all", async (req, res) => {
  var token = req.body.token;
  if (!token) {
    return res.status(401).send();
  }
  try {
    const decoded = jwt.verify(token, "leavexv1");
    try {
      var leaveregister = await LeaveRegister.find({ user_id: decoded._id });
      var leaveCount = 0;
      for (let i = 0; i < leaveregister.length; i++) {
        const leaveObject = leaveregister[i];
        var leaveType = leaveObject.leave_type;
        var leaveStatus = leaveObject.leave_status;
        var leaveDateCount = leaveObject.leave_dates.length;
        if (
          leaveType == "Half Day" &&
          (leaveStatus == "Applied" || leaveStatus == "Approved")
        ) {
          leaveDateCount = leaveDateCount * 0.5;
          leaveCount = leaveCount + leaveDateCount;
        }
        if (
          leaveType == "Casual" &&
          (leaveStatus == "Applied" || leaveStatus == "Approved")
        ) {
          leaveCount = leaveCount + leaveDateCount;
        }
        if (
          leaveType == "Sick" &&
          (leaveStatus == "Applied" || leaveStatus == "Approved")
        ) {
          leaveCount = leaveCount + leaveDateCount;
        }
        if (
          leaveType == "Work From Home" &&
          (leaveStatus == "Applied" || leaveStatus == "Approved")
        ) {
          leaveCount = leaveCount + 0;
        }
        if (
          leaveType == "Compensatory Off" &&
          (leaveStatus == "Applied" || leaveStatus == "Approved")
        ) {
          leaveCount = leaveCount + 0;
        }
        if (
          leaveType == "Admin Added" &&
          (leaveStatus == "Applied" || leaveStatus == "Approved")
        ) {
          leaveCount = leaveCount + leaveDateCount;
        }
        if (
          leaveType == "Block Leave" &&
          (leaveStatus == "Applied" || leaveStatus == "Approved")
        ) {
          leaveCount = leaveCount + leaveDateCount;
        }
        if (
          leaveType == "Paternity Leave" &&
          (leaveStatus == "Applied" || leaveStatus == "Approved")
        ) {
          leaveCount = leaveCount + leaveDateCount;
        }
        if (
          leaveType == "Maternity Leave" &&
          (leaveStatus == "Applied" || leaveStatus == "Approved")
        ) {
          leaveCount = leaveCount + leaveDateCount;
        }
        if (
          leaveType == "Sabatical Leave" &&
          (leaveStatus == "Applied" || leaveStatus == "Approved")
        ) {
          leaveCount = leaveCount + leaveDateCount;
        }
        if (
          leaveType == "Marriage Leave" &&
          (leaveStatus == "Applied" || leaveStatus == "Approved")
        ) {
          leaveCount = leaveCount + leaveDateCount;
        }
      }
      var today = new Date();
      var dd = String(today.getDate()).padStart(2, "0");
      var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
      var yyyy = today.getFullYear();
      today = yyyy + "-" + mm + "-" + dd;

      var publicholidays = await PublicHolidays.find({
        date: {
          $gte: today
        }
      })
        .sort("date")
        .limit(3);

      var isUserCarryForwardLeave = await Guser.findById(decoded._id);
      var carryForwardLeaves = isUserCarryForwardLeave.carryForwardLeaves;
      var lastYear = yyyy - 1;
      var isCarryBlockLeave = false;
      carryForwardLeaves.map(carryForwardLeave => {
        if (
          carryForwardLeave.year == lastYear &&
          carryForwardLeave.isCarryForward === true
        ) {
          isCarryBlockLeave = true;
        }
      });
      res.send({
        leaveregister: leaveregister,
        leave_count: leaveCount,
        holidays: publicholidays,
        isCarryBlockLeave
      });
      // res.send();
    } catch (error) {
      res.status(400).send(error);
    }
  } catch (error) {
    console.log(error);
    return res.status(404).send();
  }
});

router.post("/users/get-all", async (req, res) => {
  var token = req.body.token;
  try {
    const decoded = jwt.verify(token, "leavexv1");
    var user = await Guser.findById(decoded._id);
    if (!user) {
      return res.status(404).send();
    }
    let lx_users = await Guser.find();
    res.send(lx_users);
  } catch (error) {
    res.status(404).send(error);
  }
});

router.post("/leave/cancel", async (req, res) => {
  var token = req.body.token;
  var id = req.body.leave_id;
  try {
    const decoded = jwt.verify(token, "leavexv1");
    var user = await Guser.findById(decoded._id);
    if (!user) {
      return res.status(404).send();
    }
  } catch (error) {
    res.status(404).send(error);
  }
  var appliedLeave = await LeaveRegister.findById(id);
  if (!appliedLeave) {
    return res.status(400).send({ error: "leave id not found" });
  }
  appliedLeave.leave_status = "Cancelled";
  appliedLeave.cancelled_by = user._id;
  try {
    await appliedLeave.save();
    var userLeaveArray = user.leavedates;
    var userLeaveArrayNow = [];
    userLeaveArray.filter(function(obj) {
      if (obj.leave_id != id) {
        userLeaveArrayNow.push(obj);
      }
    });
    user.leavedates = userLeaveArrayNow;
    user.save();
    res.send({ cancelled: true, leave: appliedLeave });
  } catch (error) {
    console.log(error);
    return res.status(400).send(error);
  }
});

router.post("/leave/cancel/approved", auth, async (req, res) => {
  var leave_id = req.body.leave_id;
  var user = req.user;
  var cancellationMessage = req.body.cancellationMessage;
  if (cancellationMessage == "") {
    return res.send({
      error: true,
      message: "Reason for cancellation cannot be empty!"
    });
  }
  LeaveRegister.find({ _id: leave_id })
    .populate("user_id")
    .populate("approved_by")
    .exec(async (err, doc) => {
      if (err) {
        return res.status(400).send();
      }
      var userName = doc[0].user_id.userName;
      var userEmail = doc[0].user_id.userEmail;
      var leaveMessage = doc[0].leave_message;
      var leaveDates = doc[0].leave_dates;
      var pocName = doc[0].approved_by.userName;
      var pocEmail = doc[0].approved_by.userEmail;
      doc[0].cancellation_reason = cancellationMessage;
      doc[0].isCancelProcess = true;

      var leaveMailMessage =
        "Hi " +
        pocName +
        ", You got a leave cancellation request from  " +
        userName +
        " for the date(s) of " +
        leaveDates.join() +
        ".<br><br><strong>Reason to apply for leave:</strong><br>" +
        leaveMessage +
        "<br><br><strong>Reason for cancellation:</strong><br>" +
        cancellationMessage +
        '<br>Please take action request by visiting the url below <br><a href="http://leave.socialbeat.in/leave/action/' +
        leave_id +
        '">http://leave.socialbeat.in/leave/action/' +
        leave_id +
        "</a>";
      await send_cancel_mail_req(pocEmail, userName, leaveMailMessage);
      try {
        await doc[0].save();
        res.send({
          success: true,
          message: `Request has been sent to the ${pocName} for approval. You will get notified once the status change.`
        });
      } catch (error) {
        res.status(400).send(error);
      }
    });
});

router.post("/leave/approve-cancellation", auth, async (req, res) => {
  var leave_id = req.body.leave_id;
  var leaveData = await LeaveRegister.findById(leave_id).populate("user_id");

  if (!leaveData) {
    console.log("Invalid Leave ID");
    return res.status(400).send("Invalid Leave ID");
  }

  var userName = leaveData.user_id.userName;
  var userEmail = leaveData.user_id.userEmail;

  leaveData.cancelled_by = req.user._id;
  leaveData.leave_status = "Cancelled";
  leaveData.isCancelProcess = false;

  var emailSubject = "Leave Cancellation Request Status";
  var emailBody =
    "Hi " +
    userName +
    ',<br>Your leave cancellation request has been processed successfully! Please check your dashboard<br><br><br>Regards,<br>Leave X - a product of <a href="https://socialbeat.in">Social Beat</a>"';
  try {
    await leaveData.save();
    console.log({
      userEmail,
      emailSubject,
      emailBody
    });

    await send_common_mail(userEmail, emailSubject, emailBody);
    res.send();
  } catch (error) {
    console.log(error);
    res.status(400).send(error);
  }
});

router.post("/log-me-out", (req, res) => {
  console.log(req.cookies);
  res.clearCookie("vuex");
  res.send();
});

router.get("/get-emp-data", async (req, res) => {
  var user = await Guser.findOne({ userEmail: req.query.email });
  if (!user) {
    return res.send({
      statusCode: 404,
      headers: { "Content-Type": "application/json" },
      body: {
        user: false
      }
    });
  }
  res.send({
    statusCode: 200,
    headers: { "Content-Type": "application/json" },
    body: {
      name: user.userName
    }
  });
});

router.post("/get-public-holidays", async (req, res) => {
  var limit = 0;
  var match = {};
  var today = new Date();
  var dd = String(today.getDate()).padStart(2, "0");
  var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
  var yyyy = today.getFullYear();
  today = yyyy + "-" + mm + "-" + dd;

  if (req.body.limit) {
    limit = req.body.limit;
  }

  if (req.body.fromToday) {
    match = {
      date: {
        $gte: today
      }
    };
  }

  var holidays = await PublicHolidays.find(match)
    .sort("date")
    .limit(limit);
  res.send({ holidays });
});

router.post("/leave/create", async (req, res) => {
  var user = req.body.token;
  try {
    const decoded = await jwt.verify(user, "leavexv1");
    var user_id = decoded._id;
  } catch (error) {
    return res.status(400).send({ status: "error" });
  }
  var requestedUser = await Guser.findById(user_id);
  if (!requestedUser) {
    return res.status(400).send({ status: "error" });
  }
  var leave_type = req.body.leave_type;
  var color = "";
  if (leave_type == "Casual") {
    color = "#FF9800";
  } else if (leave_type == "Sick") {
    color = "#03A9F4";
  } else if (leave_type == "Compensatory off") {
    color = "#8BC34A";
  } else {
    color = "#F44336";
  }
  var leave_dates = req.body.leave_dates;
  var leave_message = req.body.leave_message;
  var leave_poc = req.body.leave_poc;
  var leave_status = "Applied";
  var year = new Date().getFullYear();
  var team = requestedUser.settings[0].team;
  var leavereq = new LeaveRegister({
    user_id,
    leave_type,
    leave_dates,
    leave_message,
    leave_poc,
    color,
    leave_status,
    year,
    team,
    isCancelProcess: false
  });
  try {
    await leavereq.save();
    requestedUser.leavedates.push({ leave_id: leavereq._id });
    await requestedUser.save();
    var recipients = leave_poc.join();
    await send_leave_apply_mail(
      requestedUser.userName,
      recipients,
      leavereq._id,
      req.body.leave_date_text,
      leave_message
    );
    res.send({ status: "ok" });
  } catch (e) {
    console.log(e);
    res.status(400).send({ status: "error" });
  }
});

router.post("/create-leads", async (req, res) => {
  var name = req.body.name;
  var email = req.body.email;
  var phone = req.body.phone;

  var leads = new Leads({
    name,
    email,
    phone
  });

  try {
    leads.save();
    res.send({ status: true });
  } catch (error) {
    res.send({ status: false });
  }
});

router.post("/leave-register", async (req, res) => {
  var token = req.body.token;
  try {
    const decoded = await jwt.verify(token, "leavexv1");
    var user_id = decoded._id;
  } catch (error) {
    console.log(error);
    return res.status(400).send({ status: "error" });
  }
  var leaveRegister = await Guser.find()
    .populate("leavedates.leave_id")
    .populate("leavedates.approved_by");
  res.send(leaveRegister);
});

router.post("/leave/approve-leave", auth, (req, res) => {
  if (req.user.role == "4") {
    return res.status(404).send();
  }

  var leave_id = req.body.leave_id;
  var leaveStatus = req.body.status;
  var approvedBy = req.user._id;
  var user_id = req.body.user_id;

  LeaveRegister.findById(leave_id, function(err, leave) {
    if (err) {
      return res.status(400).send();
    }
    leave.leave_status = leaveStatus;
    leave.approved_by = approvedBy;
    var leave_type = leave.leave_type;
    var leave_dates_taken = leave.leave_dates;
    try {
      leave.save();
      Guser.findById(user_id, async function(err, user) {
        if (err) {
          console.log(err);
          return res.status(400).send();
        }

        var userLeaveArray = user.leavedates;
        var userLeaveArrayNow = [];
        userLeaveArray.filter(function(obj) {
          if (obj.leave_id != leave_id) {
            userLeaveArrayNow.push(obj);
          } else {
            obj.approved_by = approvedBy;
            userLeaveArrayNow.push(obj);
          }
        });
        user.leavedates = userLeaveArrayNow;
        try {
          user.save();
          var leave_message =
            "Your " +
            leave_type +
            " leave for the date(s) " +
            leave_dates_taken.join() +
            " has been approved. <br><br><strong>Approved by:</b>" +
            req.user.userName;
          var email_rec = user.userEmail + ",hr@socialbeat.in";
          await send_leave_status(user.userName, email_rec, leave_message);
          res.send({
            message: "Success",
            approved_by: req.user,
            status: "Approved"
          });
        } catch (error) {
          console.log(error);
        }
      });
    } catch (error) {
      res.status(400).send();
      console.log(error);
    }
  });
});

router.post("/leave/reject-leave", auth, async (req, res) => {
  if (req.user.role == "4") {
    return res.status(404).send();
  }
  var leave_id = req.body.leave_id;
  var user_id = req.body.user_id;
  var leaveStatus = req.body.status;
  var approvedBy = req.user._id;

  LeaveRegister.findById(leave_id, function(err, leave) {
    if (err) {
      return res.status(400).send();
    }
    leave.leave_status = leaveStatus;
    leave.approved_by = approvedBy;
    var leave_type = leave.leave_type;
    var leave_dates_taken = leave.leave_dates;
    try {
      leave.save();
      Guser.findById(user_id, async (error, user) => {
        if (error) {
          return res.status(400).send();
        }
        var userLeaveArray = user.leavedates;
        var userLeaveArray;
        var userLeaveArrayNow = [];
        userLeaveArray.filter(function(obj) {
          if (obj.leave_id != leave_id) {
            userLeaveArrayNow.push(obj);
          }
        });
        user.leavedates = userLeaveArrayNow;
        try {
          user.save();
          var leave_message =
            "Your " +
            leave_type +
            " leave for the date(s) " +
            leave_dates_taken.join() +
            " has been rejected.<br><br><strong>Rejected by:</b>" +
            req.user.userName;
          await send_leave_status(user.userName, user.userEmail, leave_message);
          res.send({ message: "success", status: "Rejected" });
        } catch (user_error) {
          console.log(user_error);
          throw new Error();
        }
      });
    } catch (error) {
      console.log(error);
    }
  });
});

router.post("/attendance/get/today", auth, async (req, res) => {
  if (req.user.role == 4) {
    return res.status(404).send();
  }
  var leaves = await LeaveRegister.find()
    .populate("user_id")
    .populate("approved_by");
  res.send(leaves);
});

router.post("/employee/settings/get", auth, async (req, res) => {
  var user = await Guser.findById(req.user._id).populate(
    "settings.reporting_person"
  );
  var users = await Guser.find();
  res.send({ user, users });
});

router.post("/employee/settings/save", auth, async (req, res) => {
  var user = await Guser.findById(req.user._id);
  var reporting_person = req.body.settings.reportingPerson;

  var phone_number = req.body.settings.phone;
  var working_city = req.body.settings.workingCity;
  var dob = req.body.settings.dob;
  var blood_group = req.body.settings.blood_group;
  var team = req.body.settings.team;
  var designation = req.body.settings.designation;
  var about_me = req.body.settings.writeup;
  var doj = req.body.settings.doj;
  var poc = await Guser.find({ userEmail: reporting_person });

  var poc_id = poc[0]._id;
  var profile_status = true;
  if (
    reporting_person == "" ||
    phone_number == "" ||
    working_city == "" ||
    team == "" ||
    designation == ""
  ) {
    profile_status = false;
  }
  var newSettings = [
    {
      reporting_person: poc_id,
      phone_number,
      working_city,
      dob,
      blood_group,
      team,
      designation,
      about_me,
      doj
    }
  ];
  user.settings = newSettings;
  user.profile_status = profile_status;
  try {
    await user.save();
    res.send({ user, profile_status });
  } catch (e) {
    console.log(e);
    res.status(400).send(e);
  }
});

router.post("/employee/attendance/mark/many", auth, async (req, res) => {
  if (req.user.role == 4) {
    return res.status(404).send();
  }
  var dates = [req.body.dates];
  var reason = req.body.reason;
  var userArray = req.body.users;

  for (let i = 0; i < userArray.length; i++) {
    const email = userArray[i];
    Guser.findOne({ userEmail: email }, async (err, user) => {
      var user_id = user._id;
      var leave_type = "Admin Added";
      var leave_dates = dates;
      var leave_message = reason;
      var color = "#F44336";
      var leave_status = "Approved";
      var year = new Date().getFullYear();
      var approved_by = req.user._id;
      var newLeave = new LeaveRegister({
        user_id,
        leave_type,
        leave_dates,
        leave_message,
        color,
        leave_status,
        year,
        approved_by
      });
      try {
        await newLeave.save();
        var leave_id = newLeave._id;
        user.leavedates.push({ leave_id, approved_by });
        try {
          user.save();
        } catch (errors) {
          console.log(errors);
        }
      } catch (error) {
        console.log(error);
      }
    });
  }
  res.send();
});

router.post("/employee/attendance/mark/bulk", auth, (req, res) => {
  if (req.user.role == 4) {
    return res.status(400).send();
  }
  var dates = req.body.dates;
  var reason = req.body.reason;
  var user = req.body.user;
  Guser.findOne({ userEmail: user }, async (err, user) => {
    if (err) {
      return res.status(400).send();
    }
    var user_id = user._id;
    var leave_type = "Admin Added";
    var leave_message = reason;
    var color = "#F44336";
    var leave_status = "Approved";
    var year = new Date().getFullYear();
    var approved_by = req.user._id;
    for (let i = 0; i < dates.length; i++) {
      const leave_date = [dates[i]];
      var newLeave = new LeaveRegister({
        user_id,
        leave_type,
        leave_dates: leave_date,
        leave_message,
        color,
        leave_status,
        year,
        approved_by
      });
      try {
        newLeave.save();
        var leave_id = newLeave._id;
        user.leavedates.push({ leave_id, approved_by });
      } catch (error) {}
    }
    try {
      user.save();
      res.send();
    } catch (errors) {
      console.log(errors);
      res.status(400).send();
    }
  });
});

router.post("/leave/check/leave", async (req, res) => {
  var leave_id = req.body.leave_id;
  var leave = await LeaveRegister.findById(leave_id)
    .populate("user_id")
    .populate("approved_by");
  if (!leave) {
    res.send({ status: "invalid" });
  }
  res.send({ leave, status: "valid" });
});

router.post("/get/team-leave-register", auth, async (req, res) => {
  var pocTeam = req.user.settings[0].team;
  var leaveRegister = await Guser.find({ "settings.team": { $eq: pocTeam } })
    .populate("leavedates.leave_id")
    .populate("leavedates.approved_by");
  res.send(leaveRegister);
});

router.post("/attendance/manager/get/team/today", auth, async (req, res) => {
  var team = req.user.settings[0].team;
  var teamMembers = await Guser.find({ "settings.team": team });
  var teamMembersId = [];
  for (let i = 0; i < teamMembers.length; i++) {
    const memberId = teamMembers[i]._id;
    teamMembersId.push(memberId);
  }
  var leaves = await LeaveRegister.find({ user_id: { $in: teamMembersId } })
    .populate("user_id")
    .populate("approved_by");
  res.send(leaves);
});

router.post("/get/team/users", auth, async (req, res) => {
  var pocTeam = req.user.settings[0].team;
  var teamMembers = await Guser.find({ "settings.team": { $eq: pocTeam } });
  if (!teamMembers) {
    return res.status(404).send();
  }
  res.send(teamMembers);
});

router.post("/buddy-tracker/get/users", auth, async (req, res) => {
  Guser.find().exec(function(err, users) {
    if (err) {
      return res.status(400).send();
    }
    res.send(users);
  });
});

router.post("/buddy-tracker/get-my-buddies", auth, async (req, res) => {
  try {
    var buddies = await Guser.findById(req.user._id).populate(
      "buddies.buddy_id"
    );
    res.send(buddies);
  } catch (error) {
    res.status(400).send(error);
  }
});

router.post("/users/leave/todady", auth, async (req, res) => {
  try {
    var buddies = await Guser.find().populate("leavedates.leave_id");
    res.send(buddies);
  } catch (error) {
    res.status(400).send(error);
  }
});

router.post("/buddy-tracker/add/buddies", auth, (req, res) => {
  var user = req.user;
  var buddies = req.body.buddies;
  user.buddies = buddies;
  try {
    user.save();
    res.send({ status: "Success" });
  } catch (error) {
    console.log(error);
    res.status(400).send({ status: "Error" });
  }
});

router.post("/buddy-tracker/get/today", auth, async (req, res) => {
  var buddiesArray = req.user.buddies;
  var buddies = [];
  for (let j = 0; j < buddiesArray.length; j++) {
    const buddy_id = buddiesArray[j].buddy_id;
    buddies.push(buddy_id);
  }
  var leaves = await LeaveRegister.find({ user_id: { $in: buddies } })
    .populate("user_id")
    .populate("approved_by");
  res.send(leaves);
});

router.post("/get-poc", auth, async (req, res) => {
  var user_id = req.user._id;
  (
    await Guser.findById(user_id).populate("settings.reporting_person")
  ).execPopulate((err, user) => {
    if (err) {
      return res.status(400).send(err);
    }
    res.send(user.settings[0].reporting_person.userEmail);
  });
});

router.post("/get-monthly-report-old", auth, async (req, res) => {
  var leaveUsers = [];
  Guser.find()
    .populate("leavedates.leave_id")
    .exec((err, users) => {
      if (err) {
        return res.status(400).send(err);
      }
      for (let i = 0; i < users.length; i++) {
        const user = users[i];
        var leaveDatesArray = user.leavedates;
        if (leaveDatesArray.length > 0) {
          for (let j = 0; j < leaveDatesArray.length; j++) {
            if (
              leaveDatesArray[j].leave_id.leave_type != "Earned" ||
              leaveDatesArray[j].leave_id.leave_type != "Work From Home" ||
              leaveDatesArray[j].leave_id.leave_type != "Compensatory off"
            ) {
              if (leaveDatesArray[j].leave_id.leave_status == "Approved") {
                const leaveDates = leaveDatesArray[j].leave_id.leave_dates;
                for (let k = 0; k < leaveDates.length; k++) {
                  const leavedate = leaveDates[k];
                  var datestr = leavedate.split("-");
                  var month = datestr[1];
                  var day = datestr[2];
                  var year = datestr[0];

                  var currentdate = new Date();
                  var cur_month = currentdate.getMonth() + 1;
                  var cur_day = currentdate.getDate();
                  var cur_year = currentdate.getFullYear();

                  if (
                    cur_month == month &&
                    day >= cur_day &&
                    cur_year == year
                  ) {
                    leaveUsers.push({
                      userName: user.userName,
                      userEmail: user.userEmail,
                      dates: leavedate,
                      picture: user.picture
                    });
                  }
                }
              }
            }
          }
        }
      }
      res.send(leaveUsers);
    });
});

router.post("/get-monthly-report", (req, res) => {
  var month = parseInt(req.body.month);
  console.log(month);

  LeaveRegister.find({ leave_status: { $nin: ["Cancelled", "Rejected"] } })
    .populate("user_id")
    .exec((err, docs) => {
      if (err) {
        return res.status(400).send();
      }
      var leaveData = [];
      for (let i = 0; i < docs.length; i++) {
        const doc = docs[i];
        if (doc.user_id != null) {
          var userGroupedData = {
            name: doc.user_id.userName,
            email: doc.user_id.userEmail,
            count: 0,
            picture: doc.user_id.picture
          };
          var leaveType = doc.leave_type;
          var leaveDates = doc.leave_dates;
          for (let j = 0; j < leaveDates.length; j++) {
            const leaveDate = leaveDates[j];
            var leaveValue = 0;
            var leaveMonth = moment(leaveDate, "YYYY-MM-DD")
              .tz("Asia/Kolkata")
              .format("MM");
            if (month == leaveMonth) {
              if (leaveType == "Casual" || leaveType == "Sick") {
                leaveValue = 1;
              } else if (leaveType == "Work From Home") {
                leaveValue = 0;
              } else if (leaveType == "Compensatory off") {
                leaveValue = 0;
              } else if (leaveType == "Half Day") {
                leaveValue = 0.5;
              } else if (leaveType == "Block Leave") {
                leaveValue = 1;
              }
              userGroupedData.count = userGroupedData.count + leaveValue;
            }
          }
          leaveData.push(userGroupedData);
        }
      }
      var groupedLeaveData = leaveData.reduce(function(r, a) {
        r[a.name] = r[a.name] || [];
        r[a.name].push(a);
        return r;
      }, Object.create(null));

      var leaveUsers = Object.keys(groupedLeaveData);
      var finalGroupData = [];
      for (let l = 0; l < leaveUsers.length; l++) {
        const leaveUser = leaveUsers[l];
        var leaveUserEmail = "";
        var count = 0;
        var leaveUserPicture = "";
        var leaveGroupData = groupedLeaveData[leaveUser];
        for (let m = 0; m < leaveGroupData.length; m++) {
          const leaveGroupDataObj = leaveGroupData[m];
          leaveUserEmail = leaveGroupDataObj.email;
          leaveUserPicture = leaveGroupDataObj.picture;
          count = count + leaveGroupDataObj.count;
        }
        if (count > 0) {
          finalGroupData.push({
            name: leaveUser,
            email: leaveUserEmail,
            count,
            picture: leaveUserPicture
          });
        }
      }
      console.log(finalGroupData.length);
      res.send(finalGroupData);
    });
});

router.post("/download-monthly-report", auth, (req, res) => {
  var month = req.body.month;
  var year = req.body.year;
  var reportToSend = req.user.userEmail;
  console.log(reportToSend);
  if (month == "" || year == "") {
    return res.status(400).send();
  }
  var reqDate = year + "-" + month;
  var numbeOfDaysInCurrentMonth = moment(reqDate, "YYYY-MM").daysInMonth() + 1;
  res.send();
  try {
    generateMonthlyReportDataSend(
      month,
      year,
      numbeOfDaysInCurrentMonth,
      reportToSend
    ).then(() => {
      res.destroy();
    });
  } catch (error) {
    console.log("Couldn't process the generate request");
    console.log(error);
  }
});

function generateMonthlyReportDataSend(
  month,
  year,
  numbeOfDaysInCurrentMonth,
  reportToSend
) {
  console.log("Monthly report generation started!");
  return new Promise((resolve, reject) => {
    Guser.find(async (err, users) => {
      if (err) {
        return res.status(400).send(err);
      }
      var calendarData = [];
      for (let i = 0; i < users.length; i++) {
        const user = users[i];
        var userID = user._id;
        var userName = user.userName;
        var userSettings = user.settings[0];
        var employeeID = user.employeeID || "NA";
        if (userSettings) {
          var userJoiningDate = user.settings[0].doj;
        } else {
          var userJoiningDate = "NA";
        }
        var userExitDate = user.exitDate;
        if (!userExitDate) {
          userExitDate = "";
        }
        var userCalendarData = {
          userName,
          "Employee ID": employeeID
        };
        userCalendarData["Joining Date"] = userJoiningDate;
        userCalendarData["Exit Date"] = userExitDate;
        var userLeaves = await LeaveRegister.find({
          user_id: userID,
          leave_status: { $nin: ["Cancelled", "Rejected"] }
        }).populate("user_id");
        let monthlyLeaveCount = {
          Casual: 0,
          Sick: 0,
          "Work From Home": 0,
          "Half Day": 0,
          "Compensatory off": 0,
          "Block Leave": 0,
          "Marriage Leave": 0,
          "Sabbatical Leave": 0,
          "Paternity Leave": 0,
          "Maternity Leave": 0
        };
        for (let i = 1; i < numbeOfDaysInCurrentMonth; i++) {
          var currentI = i.toString();
          if (currentI.length == 1) {
            currentI = "0" + currentI;
          } else {
            currentI = currentI;
          }
          var currentLoopDate = year + "-" + month + "-" + currentI;
          var day = moment(currentLoopDate, "YYYY-MM-DD").format("dddd");
          var status = "P";
          for (let j = 0; j < userLeaves.length; j++) {
            const userLeave = userLeaves[j];
            var leaveType = userLeave.leave_type;
            var leaveDates = userLeave.leave_dates;
            if (leaveDates.includes(currentLoopDate)) {
              if (leaveType == "Casual") {
                status = "C";
                monthlyLeaveCount["Casual"] = monthlyLeaveCount["Casual"] + 1;
              } else if (leaveType == "Sick") {
                status = "S";
                monthlyLeaveCount["Sick"] = monthlyLeaveCount["Sick"] + 1;
              } else if (leaveType == "Half Day") {
                status = "HD";
                monthlyLeaveCount["Half Day"] =
                  monthlyLeaveCount["Half Day"] + 1;
              } else if (leaveType == "Compensatory off") {
                status = "CO";
                monthlyLeaveCount["Compensatory off"] =
                  monthlyLeaveCount["Compensatory off"] + 1;
              } else if (leaveType == "Work From Home") {
                status = "WFH";
                monthlyLeaveCount["Work From Home"] =
                  monthlyLeaveCount["Work From Home"] + 1;
              } else if (leaveType == "Block Leave") {
                status = "BL";
                monthlyLeaveCount["Block Leave"] =
                  monthlyLeaveCount["Block Leave"] + 1;
              } else if (leaveType == "Paternity Leave") {
                status = "PL";
                monthlyLeaveCount["Paternity Leave"] =
                  monthlyLeaveCount["Paternity Leave"] + 1;
              } else if (leaveType == "Maternity Leave") {
                status = "ML";
                monthlyLeaveCount["Maternity Leave"] =
                  monthlyLeaveCount["Maternity Leave"] + 1;
              } else if (leaveType == "Sabbatical Leave") {
                status = "SL";
                monthlyLeaveCount["Sabbatical Leave"] =
                  monthlyLeaveCount["Sabbatical Leave"] + 1;
              } else if (leaveType == "Marriage Leave") {
                status = "M";
                monthlyLeaveCount["Marriage Leave"] =
                  monthlyLeaveCount["Marriage Leave"] + 1;
              }
            }
          }
          if (day == "Sunday") {
            status = "Sunday";
          }
          if (day == "Saturday") {
            status = "Saturday";
          }
          let currentLoopDateFC = moment(currentLoopDate).format("DD/MM/YYYY");
          userCalendarData[currentLoopDateFC] = status;
        }

        let leaveTypeCount = {
          Casual: 0,
          Sick: 0,
          "Work From Home": 0,
          "Half Day": 0,
          "Compensatory off": 0,
          "Block Leave": 0,
          "Marriage Leave": 0,
          "Sabbatical Leave": 0,
          "Paternity Leave": 0,
          "Maternity Leave": 0
        };

        let actualLeaveTypeCount = {
          Casual: 10,
          Sick: 8,
          "Block Leave": 6,
          "Marriage Leave": 15,
          "Paternity Leave": 7
        };

        userLeaves.map(userleaveObj => {
          var userLeaveDates = userleaveObj.leave_dates;
          var userLeaveType = userleaveObj.leave_type;
          if (
            userleaveObj.leave_status == "Approved" ||
            userleaveObj.leave_status == "Applied"
          ) {
            leaveTypeCount[userLeaveType] =
              leaveTypeCount[userLeaveType] + userLeaveDates.length;
          }
        });

        let leaveBalanceBreakDown = `Casual : ${actualLeaveTypeCount["Casual"] -
          leaveTypeCount["Casual"]}, Sick : ${actualLeaveTypeCount["Sick"] -
          leaveTypeCount["Sick"]}, Block Leave : ${actualLeaveTypeCount[
          "Block Leave"
        ] -
          leaveTypeCount[
            "Block Leave"
          ]}, Marriage Leave : ${actualLeaveTypeCount["Marriage Leave"] -
          leaveTypeCount[
            "Marriage Leave"
          ]},Paternity Leave : ${actualLeaveTypeCount["Paternity Leave"] -
          leaveTypeCount["Paternity Leave"]}`;
        userCalendarData["Leave Balance"] = leaveBalanceBreakDown;

        let leaveMonthlyBreakDown = `Casual : ${monthlyLeaveCount["Casual"]}, Sick : ${monthlyLeaveCount["Sick"]}, Work From Home : ${monthlyLeaveCount["Work From Home"]}, Half Day : ${monthlyLeaveCount["Half Day"]}, Compensatory off : ${monthlyLeaveCount["Compensatory off"]}, Block Leave : ${monthlyLeaveCount["Block Leave"]}, Marriage Leave : ${monthlyLeaveCount["Marriage Leave"]}, Sabbatical Leave : ${monthlyLeaveCount["Sabbatical Leave"]}, Paternity Leave : ${monthlyLeaveCount["Paternity Leave"]}, Maternity Leave : ${monthlyLeaveCount["Maternity Leave"]}`;
        userCalendarData["Monthly Leave Count"] = leaveMonthlyBreakDown;

        let leaveBreakDown = `Casual : ${leaveTypeCount["Casual"]}, Sick : ${leaveTypeCount["Sick"]}, Work From Home : ${leaveTypeCount["Work From Home"]}, Half Day : ${leaveTypeCount["Half Day"]}, Compensatory off : ${leaveTypeCount["Compensatory off"]}, Block Leave : ${leaveTypeCount["Block Leave"]}, Marriage Leave : ${leaveTypeCount["Marriage Leave"]}, Sabbatical Leave : ${leaveTypeCount["Sabbatical Leave"]}, Paternity Leave : ${leaveTypeCount["Paternity Leave"]}, Maternity Leave : ${leaveTypeCount["Maternity Leave"]}`;
        userCalendarData["Total Leaves Taken"] = leaveBreakDown;

        calendarData.push(userCalendarData);
      }
      var sheetId = uniqid();
      let json2csvCallback = function(err, csv) {
        if (err) {
          console.log(err);
          return res.status(400).send();
        }
        fs.writeFile(
          path.join(__dirname, "../leavedata/" + sheetId + ".csv"),
          csv,
          "utf8",
          async function(err) {
            if (err) {
              console.log(
                "Some error occured - file either not saved or corrupted file saved."
              );
              await send_common_mail(
                reportToSend,
                "Report Generation Failed",
                "The report generation has been failed. Please try again."
              );
              resolve();
            } else {
              console.log("It's saved!");
              // await send_common_mail(
              //   reportToSend,
              //   "Monthly Report Generated",
              //   "Sheet Id : " + sheetId
              // );
              let transporter = nodemailer.createTransport({
                host: "smtp.gmail.com",
                port: 465,
                secure: true,
                auth: {
                  user: "hr@socialbeat.in",
                  pass: "Social@326"
                }
              });
              let info = await transporter.sendMail({
                from: "Leave X",
                to: reportToSend,
                subject: "Monthly Report Generated",
                text: "",
                html: "Hi, Please find attached the generated monthly report",
                attachments: [
                  {
                    filename: "Report.csv",
                    path: path.join(
                      __dirname,
                      "../leavedata/" + sheetId + ".csv"
                    )
                  }
                ]
              });
              resolve();
            }
          }
        );
      };
      converter.json2csv(calendarData, json2csvCallback);
    });
  });
}

router.post("/get-my-leave-dates", auth, (req, res) => {
  var user_id = req.user._id;
  LeaveRegister.find({ user_id: new ObjectId(user_id) })
    .sort({ createdAt: -1 })
    .limit(1)
    .exec((err, user) => {
      if (err) {
        return res.status(400).send(err);
      }
      res.send(user);
    });
});

router.post("/manager/get-monthly-report", auth, async (req, res) => {
  var leaveUsers = [];
  var team = req.user.settings[0].team;
  Guser.find({ "settings.team": { $eq: team } })
    .populate("leavedates.leave_id")
    .exec(async (err, users) => {
      if (err) {
        return res.status(400).send(err);
      }
      var calendarData = [];
      for (let i = 0; i < users.length; i++) {
        const user = users[i];
        var userID = user._id;
        var userName = user.userName;
        var userCalendarData = {
          userName
        };
        var userLeaves = await LeaveRegister.find({
          user_id: userID,
          leave_status: { $nin: ["Cancelled", "Rejected"] }
        }).populate("user_id");
        for (let i = 1; i < numbeOfDaysInCurrentMonth; i++) {
          var currentI = i.toString();
          if (currentI.length == 1) {
            currentI = "0" + currentI;
          } else {
            currentI = currentI;
          }
          var currentLoopDate = year + "-" + month + "-" + currentI;
          var day = moment(currentLoopDate, "YYYY-MM-DD").format("dddd");
          var status = "P";
          for (let j = 0; j < userLeaves.length; j++) {
            const userLeave = userLeaves[j];
            var leaveType = userLeave.leave_type;
            var leaveDates = userLeave.leave_dates;
            if (leaveDates.includes(currentLoopDate)) {
              if (leaveType == "Casual") {
                status = "C";
              } else if (leaveType == "Sick") {
                status = "S";
              } else if (leaveType == "Half Day") {
                status = "HD";
              } else if (leaveType == "Compensatory off") {
                status = "CO";
              } else if (leaveType == "Work From Home") {
                status = "WFH";
              } else if (leaveType == "Block Leave") {
                status = "BL";
              } else if (leaveType == "Paternity Leave") {
                status = "PL";
              }
            }
          }
          if (day == "Sunday") {
            status = "Sunday";
          }
          userCalendarData[currentLoopDate] = status;
        }
        calendarData.push(userCalendarData);
      }
      res.send(calendarData);
    });
});

router.post("/test-current-month", (req, res) => {
  var selecteddate = req.body.date;
  var datestr = selecteddate.split("-");
  var month = datestr[1];
  var day = datestr[2];
  var year = datestr[0];

  var currentdate = new Date();
  var cur_month = currentdate.getMonth() + 1;
  var cur_day = currentdate.getDate();
  var cur_year = currentdate.getFullYear();

  if (cur_month == month && day >= cur_day && cur_year == year) {
    res.send("in this month");
  } else {
    res.send("not in this month");
  }
});

router.post("/team/create-user", upload.single("avatar"), async (req, res) => {
  var profile_avatar_url = req.file.location;
  var name = req.body.name;
  var email = req.body.email;
  var team = req.body.team;
  var designation = req.body.designation;
  var designation_order =
    designation == "Head"
      ? "1"
      : designation == "EVP"
      ? "2"
      : designation == "AVP"
      ? "3"
      : designation == "Sr Manager"
      ? "4"
      : designation == "Manager"
      ? "5"
      : designation == "Lead"
      ? "6"
      : designation == "Sr Associate"
      ? "7"
      : designation == "Associate"
      ? "8"
      : "9";
  var write_up = req.body.write_up;
  var page_order = req.body.page_order;
  var newTeam = new Team({
    profile_avatar_url,
    name,
    email,
    team,
    designation,
    designation_order,
    write_up,
    page_order,
    designation_text: designation
  });
  try {
    await newTeam.save();
    res.send(newTeam);
  } catch (error) {
    res.status(400).send(error);
  }
});

router.post("/team/get-all", (req, res) => {
  // Team.find((err, users) => {
  //   if (err) {
  //     return res.status(400).send();
  //   }
  //   res.send(users);
  // }).sort({ designation_order: 1 });
  Team.find()
    .sort({ designation_order: 1 })
    .exec((err, users) => {
      if (err) {
        return Response.status(400).send(err);
      }
      res.send(users);
    });
});

router.post("/team/update-single-data", async (req, res) => {
  var id = req.body.id;
  var name = req.body.name;
  var designation = req.body.designation;
  var team = req.body.team;
  var designation_text = req.body.designation_text;
  var write_up = req.body.write_up;
  var team_class = req.body.team_class;

  Team.findById(id, async (err, docs) => {
    if (err) {
      return res.status(400).send();
    }
    var user = docs;
    user.name = name;
    user.designation = designation;
    user.team = team;
    user.designation_text = designation_text;
    user.write_up = write_up;
    user.team_class = team_class;

    try {
      await user.save();
      res.send(user);
    } catch (error) {
      res.status(400).send();
    }
  });
});

router.post(
  "/team/update-profile",
  upload.single("avatar"),
  async (req, res) => {
    var email = req.body.email;
    var team_member = await Team.findOne({ email });
    team_member.profile_avatar_url = req.file.location;
    try {
      team_member.save();
      res.send();
    } catch (error) {
      res.send(error);
    }
  }
);

router.post("/leave/check-date", auth, (req, res) => {
  LeaveRegister.find({
    user_id: new ObjectId(req.user._id),
    leave_dates: { $in: req.body.date },
    leave_status: { $nin: ["Cancelled", "Rejected"] }
  }).exec((err, dates) => {
    if (err) {
      return res.status(400).send(err);
    }
    if (dates.length > 0) {
      console.log(dates);
      return res.send({
        date: true
      });
    }
    res.send({
      date: false
    });
  });
});

router.post("/who-is-not-at-work", (req, res) => {
  var today = moment()
    .tz("Asia/Kolkata")
    .format("YYYY-MM-DD");
  LeaveRegister.find({
    leave_dates: today,
    leave_status: { $nin: ["Cancelled", "Rejected"] }
  })
    .populate("user_id")
    .exec((err, users) => {
      if (err) {
        return res.status(400).send(err);
      }
      var widgetItems = [];
      for (let i = 0; i < users.length; i++) {
        const user = users[i];
        const leaveId = user._id;
        const userName = user.user_id.userName;
        const userEmail = user.user_id.userEmail;
        const picture = user.user_id.picture;
        const leaveType = user.leave_type;
        widgetItems.push({ leaveId, userName, userEmail, picture, leaveType });
      }

      res.send(widgetItems);
    });
});

router.post("/get-monthly-report-test", (req, res) => {
  LeaveRegister.find().exec((err, users) => {
    if (err) {
      console.log(err);
      return res.status(400).send(err);
    }
    res.send(users);
  });
});

router.get("/check-connection", (req, res) => {
  res.status(400).send("Not Allowed");
});

router.post("/users/get-users-team", async (req, res) => {
  Guser.find().exec(async (err, users) => {
    if (err) {
      return res.status(400).send(err);
    }
    let userTeamArray = [];
    for (let i = 0; i < users.length; i++) {
      var user = users[i];
      let userEmail = user.userEmail;
      try {
        let userTeamData = await Team.findOne({ email: userEmail });
        if (!userTeamData) {
          console.log(`User Not Found ${userEmail}`);
          user = user.toObject();
          user.teamProfile = false;
        } else {
          user = user.toObject();
          user.teamProfile = true;
          user.teamID = userTeamData._id;
          user.userTeam = userTeamData.team;
          user.userDesignation = userTeamData.designation_text;
          user.userWriteUp = userTeamData.write_up;
        }
      } catch (error) {
        console.log(error);
      }
      userTeamArray.push(user);
    }
    res.send(userTeamArray);
  });
});

router.post("/users/get-user-by-id", (req, res) => {
  let userID = req.body.userID;
  let teamPageID = req.body.teamPageID;

  Guser.findById(userID, async (err, user) => {
    if (err) {
      return res.status(400).send(err);
    }
    if (!user) {
      return res.status(404).send();
    }
    let teamDetails = await Team.findById(teamPageID);
    user = user.toObject();
    user.teamID = teamDetails._id;
    user.userTeam = teamDetails.team;
    user.userDesignation = teamDetails.designation;
    user.userDesignationText = teamDetails.designation_text;
    user.userWriteUp = teamDetails.write_up;
    user.teamClass = teamDetails.team_class;
    user.teamPicture = teamDetails.profile_avatar_url;

    res.send(user);
  });
});

router.post("/user/update-full-profile", (req, res) => {
  let userID = req.body.userID;
  let teamPageID = req.body.teamID;
  let userSettings = req.body.userDetails.settings;
  let employeeID = req.body.userDetails.employeeID;
  let exitDate = req.body.userDetails.exitDate;
  let gender = req.body.userDetails.gender;
  let userDesignation = req.body.userDetails.userDesignation;
  let userDesignationText = req.body.userDetails.userDesignationText;
  let team = req.body.userDetails.userTeam;
  let teamClass = req.body.userDetails.teamClass;
  let userWriteUp = req.body.userDetails.userWriteUp;

  Guser.findByIdAndUpdate(
    userID,
    { settings: userSettings, employeeID, exitDate, gender },
    (err, updated) => {
      if (err) {
        console.log(err);
        return res.status(400).send(err);
      }

      Team.findByIdAndUpdate(
        teamPageID,
        {
          designation: userDesignation,
          designation_text: userDesignationText,
          team,
          team_class: teamClass,
          write_up: userWriteUp
        },
        (err, updated) => {
          if (err) {
            console.log(err);
            return res.status(400).send(err);
          }

          res.send();
        }
      );
    }
  );
});

router.post("/user/create-team-profile", (req, res) => {
  let userID = req.body.userDetails.userID;
  Guser.findById(userID, async (err, user) => {
    let settings = user.settings;
    let isSettings = null;
    if (settings) {
      if (settings[0]) {
        isSettings = true;
      } else {
        isSettings = false;
      }
    } else {
      isSettings = false;
    }

    if (isSettings) {
      settings[0].doj = req.body.userDetails.settings[0].doj;
      Guser.findByIdAndUpdate(userID, { settings }, (err, updated) => {
        if (err) {
          console.log(err);
        }
      });
    } else {
      let userSettings = [
        {
          doj: req.body.userDetails.settings[0].doj,
          team: req.body.userDetails.userTeam,
          designation: req.body.userDetails.userDesignation
        }
      ];
      Guser.findByIdAndUpdate(
        userID,
        { settings: userSettings },
        (err, updated) => {
          if (err) {
            console.log(err);
          }
        }
      );
    }

    let order = Math.floor(Math.random() * 10);

    let teamProfile = new Team({
      name: req.body.userDetails.userName,
      email: req.body.userDetails.userEmail,
      team: req.body.userDetails.userTeam,
      designation: req.body.userDetails.userDesignation,
      designation_order: order,
      write_up: req.body.userDetails.userWriteUp,
      page_order: order,
      designation_text: req.body.userDetails.userDesignationText,
      team_class: req.body.userDetails.teamClass
    });

    try {
      await teamProfile.save();
      res.send(teamProfile._id);
    } catch (error) {
      console.log(error);
      res.status(400).send(err);
    }
  });
});

router.post(
  "/user/update-profile-pic",
  upload.single("employee-picture"),
  (req, res) => {
    let teamID = req.body.teamID;
    let file = req.file.location;
    Team.findByIdAndUpdate(
      teamID,
      { profile_avatar_url: file },
      (err, updated) => {
        if (err) {
          console.log(err);
          return res.status(400).send(err);
        }
        res.send("Picture Updated");
      }
    );
  }
);

router.post("/delete/user", (req, res) => {
  let userID = req.body.userID;
  let isTeamProfile = req.body.isTeamProfile;
  let userEmail = req.body.userEmail;

  LeaveRegister.find({ user_id: userID }, (err, docs) => {
    if (err) {
      console.log("Couldn't find leave register " + userID);
      return res.status(400).send();
    }

    docs.map(async leaveDoc => {
      try {
        var deleteReq = await LeaveRegister.findByIdAndDelete(leaveDoc._id);
      } catch (error) {
        console.log("Couldn't delete");
        console.log(error);
        return res.status(400).send(error);
      }
    });

    Guser.findByIdAndDelete(userID, (err, deleted) => {
      if (err) {
        console.log(err);
        return res.status(400).send(err);
      }
    });
  });

  if (isTeamProfile) {
    let teamID = req.body.teamID;
    Team.findByIdAndDelete(teamID, (err, deleted) => {
      if (err) {
        console.log("Error deleting team profile " + teamID);
      }
    });
  }

  res.send({ success: true, userEmail });
});

router.post("/users/get-user-profile", (req, res) => {
  let userID = req.body.userID;
  Guser.findById(userID, (err, user) => {
    if (err) {
      return res.status(400).send(err);
    }
    res.send(user);
  });
});

// router.post("/add-iscancell-process", (req, res) => {
//   LeaveRegister.find(async (err, docs) => {
//     if (err) {
//       return res.status(400).send();
//     }
//     for (let i = 0; i < docs.length; i++) {
//       const doc = docs[i];
//       doc.isCancelProcess = false;
//       try {
//         await doc.save();
//       } catch (error) {
//         console.log(error);
//       }
//     }
//     res.send();
//   });
// });

router.post("/check-settings/v2", (req, res) => {
  Guser.find(async (err, users) => {
    for (let i = 0; i < users.length; i++) {
      const user = users[i];
      var settings = user.settings;
      if (settings === undefined || settings.length === 0) {
        console.log(user.userEmail);
        // user.settings = [{ doj: "" }];
        // try {
        //   await user.save();
        // } catch (error) {
        //   console.log(error);
        // }
      }
    }
  });
  res.send();
});

// router.post(
//   "/update-employee-id",
//   upload.single("employee-csv"),
//   (req, res) => {
//     var file = req.file.location;
//     var usersData = [];
//     csv()
//       .fromStream(request.get(file))
//       .subscribe(
//         json => {
//           return new Promise(async (resolve, reject) => {
//             usersData.push(json);
//             resolve();
//           });
//         },
//         onError,
//         onComplete
//       );

//     function onError() {
//       return res.status(400).send();
//     }

//     async function onComplete() {
//       var errorArray = [];
//       var userNotFound = [];
//       if (usersData.length > 0) {
//         for (let i = 0; i < usersData.length; i++) {
//           const userDetail = usersData[i];
//           var email = userDetail.email.toLowerCase();
//           var employee = await Guser.findOne({ userEmail: email });
//           if (!employee) {
//             console.log("Employee not found " + email);
//           } else {
//             var employeeSettings = employee.settings;
//             employeeSettings[0].doj = userDetail.doj;
//             employee.employeeID = userDetail.empID;
//             if (userDetail.isCarryForward == "Yes") {
//               employee.carryForwardLeaves = [
//                 { year: "2019", isCarryForward: true }
//               ];
//             }
//             employee.exitDate = userDetail.exitDate;

//             try {
//               employee.save();
//             } catch (error) {
//               console.log(error);
//             }
//           }
//         }
//       }
//       res.send({ errorArray, userNotFound });
//     }
//   }
// );

router.post(
  "/update-employee-gender",
  upload.single("employee-csv"),
  (req, res) => {
    var file = req.file.location;
    var usersData = [];
    csv()
      .fromStream(request.get(file))
      .subscribe(
        json => {
          return new Promise(async (resolve, reject) => {
            console.log(json);
            usersData.push(json);
            resolve();
          });
        },
        onError,
        onComplete
      );

    function onError() {
      return res.status(400).send();
    }

    async function onComplete() {
      if (usersData.length > 0) {
        for (let i = 0; i < usersData.length; i++) {
          const userDetail = usersData[i];
          var email = userDetail.emailID.toLowerCase();
          var employee = await Guser.findOne({ userEmail: email });
          if (!employee) {
            console.log("Employee not found " + email);
          } else {
            employee.gender = userDetail.gender;
            try {
              await employee.save();
            } catch (error) {
              console.log("Couldn't save " + email);
            }
          }
        }
      }
      res.send();
    }
  }
);

router.post("/feedback/record-feedback", (req, res) => {
  let userEmail = req.body.email;
  let feedback = req.body.feedback;
  Guser.findOne({ userEmail: userEmail }, async (err, user) => {
    if (err) {
      return res.status(400).send(err);
    }
    let userID = user._id;
    var newFeedback = new Feedback({
      user_id: userID,
      feedback: feedback
    });
    try {
      await newFeedback.save();
      res.send();
    } catch (error) {
      console.log(error);
      res.status(400).send(error);
    }
  });
});

router.post("/feedback/check-monthly-feedback", (req, res) => {
  let userEmail = req.body.email;
  Guser.findOne({ userEmail: userEmail }, async (err, user) => {
    if (err) {
      return res.status(400).send(err);
    }
    let userID = user._id;
    Feedback.find(
      {
        user_id: userID
      },
      (err, feedbacks) => {
        if (err) {
          return res.status(400).send(err);
        }
        let currentMonth = moment().format("MM");
        let isFeedback = false;
        feedbacks.map(feedback => {
          let createdDate = feedback.createdAt;
          if (moment(createdDate).format("MM") == currentMonth) {
            isFeedback = true;
          }
        });
        res.send({ isFeedback });
      }
    );
  });
});

router.post("/feedbacks/get", (req, res) => {
  Feedback.find({})
    .populate("user_id")
    .exec((err, feedbacks) => {
      if (err) {
        return res.status(400).send(err);
      }
      let sanitizedFeedbacks = [];
      feedbacks.map(feedback => {
        if (feedback.user_id) {
          sanitizedFeedbacks.push({
            feedback: feedback.feedback,
            createdAt: moment(feedback.createdAt)
              .tz("Asia/Kolkata")
              .format("DD-MM-YYYY, h:mm a"),
            createdBy: feedback.user_id.userName,
            userEmail: feedback.user_id.userEmail
          });
        }
      });
      res.send(sanitizedFeedbacks);
    });
});

router.post("/feedbacks/download", (req, res) => {
  var reportToSend = req.body.userEmail;
  Feedback.find({})
    .populate("user_id")
    .exec((err, feedbacks) => {
      if (err) {
        return res.status(400).send(err);
      }
      let sanitizedFeedbacks = [];
      feedbacks.map(feedback => {
        if (feedback.user_id) {
          sanitizedFeedbacks.push({
            feedback: feedback.feedback,
            createdAt: moment(feedback.createdAt)
              .tz("Asia/Kolkata")
              .format("DD-MM-YYYY, h:mm a"),
            createdBy: feedback.user_id.userName,
            userEmail: feedback.user_id.userEmail
          });
        }
      });

      var sheetId = uniqid();
      let json2csvCallback = function(err, csv) {
        if (err) {
          console.log(err);
          return res.status(400).send();
        }
        fs.writeFile(
          path.join(__dirname, "../leavedata/" + sheetId + ".csv"),
          csv,
          "utf8",
          async function(err) {
            if (err) {
              console.log(
                "Some error occured - file either not saved or corrupted file saved."
              );
              await send_common_mail(
                reportToSend,
                "Feedback Generation Failed",
                "The report generation has been failed. Please try again."
              );
              resolve();
            } else {
              console.log("It's saved!");
              let transporter = nodemailer.createTransport({
                host: "smtp.gmail.com",
                port: 465,
                secure: true,
                auth: {
                  user: "hr@socialbeat.in",
                  pass: "Social@326"
                }
              });
              let info = await transporter.sendMail({
                from: "Leave X",
                to: reportToSend,
                subject: "Feedback Report Generated",
                text: "",
                html: "Hi, Please find attached the generated feedbacks",
                attachments: [
                  {
                    filename: "Feedbacks.csv",
                    path: path.join(
                      __dirname,
                      "../leavedata/" + sheetId + ".csv"
                    )
                  }
                ]
              });
              res.send();
            }
          }
        );
      };
      converter.json2csv(sanitizedFeedbacks, json2csvCallback);
    });
});

// router.post("/change-joining-date-format", (req, res) => {
//   Guser.find(async (err, users) => {
//     if (err) {
//       return res.status(400).send(err);
//     }

//     users.map(async user => {
//       let userID = user._id;
//       try {
//         // let userSettings = user.settings[0];
//         // let userDOJ = userSettings.doj;
//         // if (userDOJ != "") {
//         //   let userDOJCon = moment(userDOJ, "DD/MM/YYYY").format("YYYY-MM-DD");
//         //   console.log(userDOJCon);
//         //   userSettings.doj = userDOJCon;
//         //   try {
//         //     let updatedUser = await Guser.findByIdAndUpdate(userID, {
//         //       settings: userSettings
//         //     });
//         //   } catch (error) {
//         //     console.log("Update failed for " + user.userEmail);
//         //   }
//         // }

//         let userED = user.exitDate;
//         if (userED != "") {
//           let userEDCon = moment(userED, "DD/MM/YYYY").format("YYYY-MM-DD");
//           try {
//             let updatedUser = await Guser.findByIdAndUpdate(userID, {
//               exitDate: userEDCon
//             });
//           } catch (error) {
//             console.log("Update failed for " + user.userEmail);
//           }
//         }
//       } catch (error) {
//         console.log("Not defined");
//         console.log(user.userEmail);
//       }
//     });

//     res.send();
//   });
// });

module.exports = router;
