const nodemailer = require("nodemailer");
var send_leave_apply_mail = async function(
  user_name,
  email,
  leave_id,
  leave_dates,
  leave_message
) {
  let transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 465,
    secure: true,
    auth: {
      user: "hr@socialbeat.in",
      pass: "Social@326"
    }
  });
  let info = await transporter.sendMail({
    from: "SB Leave X",
    to: email,
    subject: "SB Leave X - Leave request from " + user_name,
    text: "",
    html:
      "Hi, You got a new leave request from  " +
      user_name +
      " for the date(s) of " +
      leave_dates +
      ".<br><br><strong>Reason:</strong><br>" +
      leave_message +
      '<br><br>Please take action request by visiting the url below <br><a href="http://leave.socialbeat.in/leave/action/' +
      leave_id +
      '">http://leave.socialbeat.in/leave/action/' +
      leave_id +
      "</a>"
  });

  console.log(info.messageId);
};

module.exports = send_leave_apply_mail;
