const nodemailer = require("nodemailer");
var send_leave_status = async function(user_name, email, leave_message) {
  let transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 465,
    secure: true,
    auth: {
      user: "hr@socialbeat.in",
      pass: "Social@326"
    }
  });
  let info = await transporter.sendMail({
    from: "Leave X",
    to: email,
    subject: "Leave X - Leave request status",
    text: "",
    html:
      "Hi " +
      user_name +
      ",<br>" +
      leave_message +
      '<br><br>Regards,<br>Leave X - a product of <a href="https://socialbeat.in">Social Beat</a>'
  });
};

module.exports = send_leave_status;
