import { Pie, mixins } from "vue-chartjs";

export default {
  extends: Pie,
  mixins: [mixins.reactiveProp],
  props: ["data", "chartData", "options"],
  mounted() {
    this.renderChart(this.data, this.options);
  }
};
