import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";
import * as Cookies from "js-cookie";

Vue.use(Vuex);

const store = () =>
  new Vuex.Store({
    state: () => ({
      user_logged_in: false,
      user_name: "",
      user_email: "",
      token: "",
      avatar: "",
      app_id: [],
      role: "",
      profile_status: "",
      timer: {
        status: false,
        startTime: ""
      },
      isFeedbackAccess: false
    }),

    mutations: {
      setUserState(state, userData) {
        state.user_logged_in = userData.loggedin;
        state.user_name = userData.user.userName;
        state.user_email = userData.user.userEmail;
        state.token = userData.user.token;
        state.avatar = userData.user.picture;
        state.app_id = userData.user.app_id;
        state.role = userData.user.role;
        state.profile_status = userData.user.profile_status;
      },
      setUserProfileState(state, profile_state) {
        state.profile_status = profile_state;
      },
      setUserTimerState(state, timerState) {
        state.timer.status = timerState.status;
        state.time.startTime = timerState.startTime;
      },
      setFeebackAccess(state, access) {
        state.isFeedbackAccess = access;
      }
    },

    actions: {
      nuxtServerInit({ commit }, { req }) {
        if (req.cookies && req.cookies.vuex) {
          var userCookies = req.cookies.vuex;
          userCookies = JSON.parse(userCookies);
          var userData = {
            loggedin: userCookies.user_logged_in,
            user: {
              userName: userCookies.user_name,
              userEmail: userCookies.user_email,
              token: userCookies.token,
              picture: userCookies.avatar,
              app_id: userCookies.app_id,
              role: userCookies.role,
              profile_status: userCookies.profile_status
            }
          };
          commit("setUserState", userData);
          var feedbackAccess = userCookies.isFeedbackAccess;
          commit("setFeebackAccess", feedbackAccess);
          // var timerData = userCookies.timer;
          // commit("setUserTimerState", timerData);
        }
      }
    },
    getters: {
      userState: state => {
        return state.user_logged_in;
      }
    },
    plugins: [
      createPersistedState({
        storage: {
          getItem: key => Cookies.get(key),
          setItem: (key, value) =>
            Cookies.set(key, value, { expires: 3, secure: false }),
          removeItem: key => Cookies.remove(key)
        }
      })
    ]
  });

export default store;
