import axios from 'axios';
export default async function ({ app, store, redirect }) {
    var cookietok = app.$cookies.get('leavetoken');
    if (!cookietok) {
        return redirect('/')
    }
    if (cookietok) {
        await axios.post('http://localhost:3000/api/check-auth', {
            token: cookietok
        }).then((response) => {
            app.$cookies.set('user-data', response.data);
        }).catch((e) => {
            console.log(e);
        })
    }

}