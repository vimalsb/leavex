export default function ({ app, redirect }) {
    var user = app.$cookies.get("user-data");
    if (!user) {
        return redirect("/");
    }
}