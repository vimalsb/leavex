export default function ({ store, redirect, context }) {
    if (!store.state.user_logged_in) {
        return redirect('/')
    }
}