export default function ({ store, redirect, route }) {
    if (!store.state.user_logged_in || store.state.role != "1") {
        return redirect('/?to=' + route.fullPath)
    }
}